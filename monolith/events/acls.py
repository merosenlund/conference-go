import requests

from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_image_url(location_name):
    header = {"Authorization": PEXELS_API_KEY}
    payload = {"query": location_name}
    pexels = requests.get(
        "https://api.pexels.com/v1/search", headers=header, params=payload
    ).json()
    photos = pexels["photos"]
    image_url = photos[0]["url"]
    return image_url


def get_city_weather(city_name):
    payload = {
        "q": city_name,
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    coords = requests.get(url, params=payload).json()[0]

    payload = {
        "lat": coords["lat"],
        "lon": coords["lon"],
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    weather = requests.get(url, params=payload).json()
    temp = weather["main"]["temp"]
    description = weather["weather"][0]["description"]

    return {
        "temp": temp,
        "description": description,
    }
